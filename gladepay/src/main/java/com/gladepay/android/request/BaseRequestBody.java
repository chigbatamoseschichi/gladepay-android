package com.gladepay.android.request;

import android.provider.Settings;

import com.gladepay.android.GladepaySdk;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;


/**
 * A base for all request bodies
 */
abstract class BaseRequestBody {
    static final String FIELD_DEVICE = "device";
    @SerializedName(FIELD_DEVICE)
    String device;

    public abstract JsonObject getInitiateParamsJsonObjects();

    void setDeviceId() {
        this.device = "androidsdk_" + Settings.Secure.getString(GladepaySdk.applicationContext.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

}
