package com.gladepay.android;

import com.google.gson.JsonObject;

public class PTransaction {
    private String transactionRef;
    private String statusId;
    private String txnStatus;
    private String message = "";


    public String getTransactionRef() {
        return transactionRef;
    }

    public void loadFromResponse(JsonObject jsonObject) {
        if(jsonObject.has("status") && jsonObject.has("txnRef")){
            this.transactionRef = jsonObject.get("txnRef").getAsString();
            this.statusId = jsonObject.get("status").getAsString();
        }

        if(jsonObject.has("txnStatus")){
            this.txnStatus = jsonObject.get("txnStatus").getAsString();
        }
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setTxnStatus(String status) {
        this.txnStatus = status;
    }

    public String getStatusId() {
        return statusId;
    }

    public String getTxnStatus() {
        return txnStatus;
    }

    boolean hasStartedProcessingOnServer() {
        return (this.transactionRef != null) && ( statusId != null );
    }
}
