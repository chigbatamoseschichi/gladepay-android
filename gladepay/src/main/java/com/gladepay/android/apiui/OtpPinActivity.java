package com.gladepay.android.apiui;


import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.gladepay.android.R;

import co.paystack.android.design.widget.PinPadView;

public class OtpPinActivity extends Activity {

    final AuthSingleton si = AuthSingleton.getInstance();
    private PinPadView pinpadView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


//        setTitle("ENTER OTP");
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        String auth_type = getIntent().getStringExtra("auth_type");


        if (auth_type.equalsIgnoreCase("otp")){
            setContentView(R.layout.gladepay_otp_activity);
            OTP();
        } else if(auth_type.equalsIgnoreCase("pin")){
            setContentView(R.layout.co_gladepay___activity_otp);
            pinpadView = findViewById(R.id.pinpadView);
            setup();
        }

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    protected void OTP(){
        String validateMessage = getIntent().getStringExtra("message");
        //////// Using EditText
        final EditText otpEditText = findViewById(R.id.otp_edittext);
        TextView otpMessage = findViewById(R.id.otp_message_textview);
        otpMessage.setText(validateMessage);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE); // show keyboard

        Button submitButton = findViewById(R.id.submit_button);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String otpString;
                otpString = otpEditText.getText().toString();
                Log.v("Otp", otpString);
                handleSubmit("otp", otpString);
            }
        });
    }

    protected void setup() {


//        pinpadView.setPromptText(si.getAuthMessage());
        pinpadView.setPromptText("Input PIN");
        pinpadView.setVibrateOnIncompleteSubmit(false);
        pinpadView.setAutoSubmit(false);

        pinpadView.setOnPinChangedListener(new PinPadView.OnPinChangedListener() {
            @Override
            public void onPinChanged(String oldPin, String newPin) {
                // We had set length to 10 while creating,
                // but in case some otp is longer,
                // we will keep increasing pin length
                if(newPin.length() >= pinpadView.getPinLength()){
                    pinpadView.setPinLength(pinpadView.getPinLength()+1);
                }
            }
        });

        pinpadView.setOnSubmitListener(new PinPadView.OnSubmitListener() {
            // Always submit (we never expect this to complete since
            // we keep increasing the pinLength during pinChanged event)
            // we still handle onComplete nonetheless
            @Override
            public void onCompleted(String pin) {
                handleSubmit("pin", pin);
            }

            @Override
            public void onIncompleteSubmit(String pin) {
                handleSubmit("pin", pin);
            }
        });
    }

    public void onDestroy() {
        super.onDestroy();
        handleSubmit("otp" ,"");
    }

    public void handleSubmit(String mode, String code){
        synchronized (si) {
            if (mode.equalsIgnoreCase("otp")){
                si.setOtp(code);
            } else if (mode.equalsIgnoreCase("pin")){
                si.setPin(code);
            }
            si.notify();
        }
        finish();
    }
}
