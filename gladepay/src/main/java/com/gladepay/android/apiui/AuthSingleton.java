package com.gladepay.android.apiui;

public class AuthSingleton {
    private static AuthSingleton instance = new AuthSingleton();
    private String otp = "";
    private String authMessage = "";
    private String pin = "";

    private AuthSingleton() {
    }

    public static AuthSingleton getInstance() {
        return instance;
    }

    public String getAuthMessage() {
        return authMessage;
    }

    public String getPin() {return pin;}

    public AuthSingleton setAuthMessage(String authMessage) {
        this.authMessage = authMessage;
        return this;
    }

    public String getOtp() {
        return otp;
    }

    public AuthSingleton setOtp(String otp) {
        this.otp = otp;
        return this;
    }

    public AuthSingleton setPin(String pin){
        this.pin = pin;
        return this;
    }
}
