package com.gladepay.android.apiui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ViewGroup;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.gladepay.android.R;

public class Auth3DActivity extends AppCompatActivity {
    WebView webView;
    String url;
    Intent intent;
    final AuthSingleton si = AuthSingleton.getInstance();
    String jsonResp;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gladepay_3d_activity);
        webView = findViewById(R.id.webview_layout);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new Browser());

        webView.setWebChromeClient(new WebChromeClient(){
            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                Log.v("Console Message:", consoleMessage.message());
                jsonResp = consoleMessage.message();
                synchronized (si){
                    si.setAuthMessage(jsonResp);
                    si.notify();
                }
                finish();
                return super.onConsoleMessage(consoleMessage);
            }
        });

        intent = getIntent();
        url = intent.getStringExtra("url");
        Log.v("Auth3D: ", url);
        webView.loadUrl(url.replaceAll("\"", ""));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.v("Auth3d", "On destroy called");
        if (webView != null) {
            // the WebView must be removed from the view hierarchy before calling destroy
            // to prevent a memory leak
            // See https://developer.android.com/reference/android/webkit/WebView.html#destroy%28%29
            ((ViewGroup) webView.getParent()).removeView(webView);
            webView.removeAllViews();
            webView.destroy();
            webView = null;
        }
    }

    private class Browser extends WebViewClient{

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if (url.contains("APPROVED")){
                webView.loadUrl("javascript:console.log(document.getElementsByTagName(\"body\")[0].innerText);");
            } else if (url.contains("DECLINED")){
                webView.loadUrl("javascript:console.log(document.getElementsByTagName(\"body\")[0].innerText);");
            }
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            Log.v("AuthURL", url);
            if (url.contains("APPROVED")){
                Log.v("AuthURL", "APPROVED");
                return true;

            } else if (url.contains("DECLINED")){
                Log.v("AuthURL", "DECLINED");
                return true;
            }
            return false;
//            return super.shouldOverrideUrlLoading(view, request);
        }
    }
}
